package com.debugers.healthcare.medicineintakereminder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;




import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class ReminderDateSortListAdapter extends ArrayAdapter<NotificationDetailsPack> {
	
	Context context;
	
	
	
	
	SimpleDateFormat timeView = new SimpleDateFormat("hh:mm aa");
	
	ArrayList<NotificationDetailsPack> reminder_list;




	

	public ReminderDateSortListAdapter(Context context, ArrayList<NotificationDetailsPack> reminder_list) {
		super(context,R.layout.date_sort_list_view,reminder_list);
		this.reminder_list=reminder_list;
		this.context=context;
		
		// TODO Auto-generated constructor stub
	}

	static class ViewHolder {
	   
	    public ImageView date_sort_list_photo_of_medicine;
	    public TextView date_sort_list_reminder_satus;
	    public TextView date_sort_list_med_name;
	    public TextView date_sort_list_taker_name;
	    public TextView date_sort_list_take_size;
		public TextView date_sort_list__item_time;
	    public TextView date_sort_delay_alert_text;
	    
	  }

	
	  

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    View rowView = convertView;
	    
	    
	    if (rowView == null) {
	    	
	    	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	
	      rowView = inflater.inflate(R.layout.date_sort_list_view, null);
	      
	      ViewHolder viewHolder = new ViewHolder();
	     
	      viewHolder.date_sort_list_photo_of_medicine = (ImageView) rowView.findViewById(R.id.date_sort_list_photo_of_medicine);
	      viewHolder.date_sort_list_reminder_satus = (TextView) rowView.findViewById(R.id.date_sort_list_reminder_satus);
	      viewHolder.date_sort_list_med_name = (TextView) rowView.findViewById(R.id.date_sort_list_med_name);
	      viewHolder.date_sort_list_taker_name = (TextView) rowView.findViewById(R.id.date_sort_list_taker_name);
	      viewHolder.date_sort_list_take_size = (TextView) rowView.findViewById(R.id.date_sort_list_take_size);
	      viewHolder.date_sort_list__item_time = (TextView) rowView.findViewById(R.id.date_sort_list__item_time);
	      viewHolder.date_sort_delay_alert_text = (TextView) rowView.findViewById(R.id.date_sort_delay_alert_text);
	      rowView.setTag(viewHolder);
	    }
	    
	    

	    final ViewHolder holder = (ViewHolder) rowView.getTag();
	  
    	holder.date_sort_list_med_name.setText(reminder_list.get(position).getReminderDataPack().getMedicine_name());
    	holder.date_sort_list_take_size.setText(reminder_list.get(position).getReminderDataPack().getQuantity_per_time()
    			+" "+context.getResources().getStringArray(R.array.takePerDay)[reminder_list.get(position).getReminderDataPack().getTake_form_type_index()]);
    	
    	holder.date_sort_list_taker_name.setText(reminder_list.get(position).getReminderDataPack().getTaker_name());
    	
    	holder.date_sort_list_photo_of_medicine.setImageURI(Uri.parse(reminder_list.get(position).getReminderDataPack().getPhoto_name()));
    	
    		
    	 holder.date_sort_list__item_time.setText(timeView.format(reminder_list.get(position).getNotification_time())+reminder_list.get(position).getDelay_time());
    			
    	 holder.date_sort_delay_alert_text.setVisibility(View.GONE);
    	 
    	 if(reminder_list.get(position).getReminder_status()==1){
    		 holder.date_sort_delay_alert_text.setVisibility(View.VISIBLE);
    		 
    		 String msg="";
    		 if(reminder_list.get(position).getDelay_time()>0){
    			 msg="Delay for take medicine" +timeView.format(reminder_list.get(position).getDelay_time());
    			
 	    	}
    		 
    		 msg+="Take after feel is"+reminder_list.get(position).getTaker_feel();
    		 
    		 holder.date_sort_delay_alert_text.setText(msg);
    		 
    	 }
	    	
	    	
	    	
	    	holder.date_sort_list_photo_of_medicine.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					final Dialog photo_dialog = new Dialog(context);
					photo_dialog.setTitle("Medicine Photo View");
					photo_dialog.setContentView(R.layout.dialog_medicine_photo_full_view);
					
			    	ImageView dialog_medicine_full_view_photo=(ImageView) photo_dialog.findViewById(R.id.dialog_medicine_full_view_photo);
			    	Button dialog_full_view_btn =(Button) photo_dialog.findViewById(R.id.dialog_full_view_btn);
			    	dialog_medicine_full_view_photo.setImageURI(Uri.parse(reminder_list.get(position).getReminderDataPack().getPhoto_name()));
			    	
			    	dialog_full_view_btn.setOnClickListener(new View.OnClickListener() {

			            @Override
			            public void onClick(View v) {
			                // TODO Auto-generated method stub
			            	
			            	photo_dialog.cancel();
			               


			            }
			        });
			    	
			    	
			    	photo_dialog.show();
					
				}
			});
	    	
			
		
	   
		
	    
	    
	   

	    return rowView;
	  
	} 
	
	
}
