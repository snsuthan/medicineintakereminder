package com.debugers.healthcare.medicineintakereminder;




import java.util.ArrayList;

import com.debugers.healthcare.medicineintakereminder.R;
import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends Activity {
	
	
	
	ListView listview ;
	ArrayList<NotificationDetailsPack> reminder_list;
	
	ViewReminder view_reminder;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        
        listview = (ListView) findViewById(R.id.main_upcoming_list_view);
        
        view_reminder=new ViewReminder(getBaseContext());
        
        reminder_list=view_reminder.getNotificationList(System.currentTimeMillis());
        
        if(reminder_list.size()==0){
        	
        		
        }
        
		ReminderDateSortListAdapter listArrayAdapter=new ReminderDateSortListAdapter(getApplicationContext(), reminder_list);
		
		listview.setAdapter(listArrayAdapter);
		
		
		
	}
		
		
            
       
    public void onButtonClick(View view){
    	
    	
    	
    	switch (view.getId()) {
		case R.id.main_new_reminder_btn:
			
			Intent n_intent=new Intent(getApplicationContext(),NewReminderActivity.class);
			 startActivity(n_intent);
			
			
			break;
			
			
		case R.id.main_view_reminder_btn:
			
			Intent v_intent=new Intent(getApplicationContext(),ReminderMainListActivity.class);
			 startActivity(v_intent);
			
			break;
			
			


		default:
			break;
		}
    	
    	
    }
		
		
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		return true;
	}

	
	
	


}