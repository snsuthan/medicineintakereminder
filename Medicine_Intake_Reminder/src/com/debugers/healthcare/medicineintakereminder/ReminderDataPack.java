package com.debugers.healthcare.medicineintakereminder;

import java.io.Serializable;



@SuppressWarnings("serial")
public class ReminderDataPack implements Serializable{
	
	String 	taker_name="",
			medicine_name="",
			photo_name="";
	
	long  	start_date,
			reminder_ld,
	      	end_date;
	
	
	int 	schedule_type_index,
			take_form_type_index,
			quantity_per_time;
	
	
	String 	schedule_data;


	public String getTaker_name() {
		return taker_name;
	}


	public void setTaker_name(String taker_name) {
		this.taker_name = taker_name;
	}


	public String getMedicine_name() {
		return medicine_name;
	}


	public void setMedicine_name(String medicine_name) {
		this.medicine_name = medicine_name;
	}


	public long getReminder_ld() {
		return reminder_ld;
	}


	public void setReminder_ld(long reminder_Id) {
		this.reminder_ld = reminder_Id;
	}


	public String getPhoto_name() {
		return photo_name;
	}


	public void setPhoto_name(String photo_Name) {
		this.photo_name = photo_Name;
	}


	public long getStart_date() {
		return start_date;
	}


	public void setStart_date(long start_date) {
		this.start_date = start_date;
	}


	public long getEnd_date() {
		return end_date;
	}


	public void setEnd_date(long end_date) {
		this.end_date = end_date;
	}


	public int getSchedule_type_index() {
		return schedule_type_index;
	}


	public void setSchedule_type_index(int schedule_type_index) {
		this.schedule_type_index = schedule_type_index;
	}


	public int getTake_form_type_index() {
		return take_form_type_index;
	}


	public void setTake_form_type_index(int take_form_type_index) {
		this.take_form_type_index = take_form_type_index;
	}


	public int getQuantity_per_time() {
		return quantity_per_time;
	}


	public void setQuantity_per_time(int quantity_per_time) {
		this.quantity_per_time = quantity_per_time;
	}


	public String getSchedule_data() {
		return schedule_data;
	}


	public void setSchedule_data(String schedule_data) {
		this.schedule_data = schedule_data;
	}


	

	
	
	
	
	

}
