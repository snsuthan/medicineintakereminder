package com.debugers.healthcare.medicineintakereminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

@SuppressLint("SimpleDateFormat")
public class ReminderNameSortListViewAdapter extends ArrayAdapter<ReminderDataPack> {
	
	
	public final static String TAG_FILTER_OPTION="filter";
	
	private final Context context;
	  private final ArrayList<ReminderDataPack> objects;
	  ViewReminder db;
	  String reminder_status,reminder_time_list_set;
	  
	  String filter_option;
	  
	  public final static String TAG_TAKER_NAME_CASE="taker_name";
		public final static String TAG_MEDICINE_NAME_CASE="medicine_name";
	 
	  SimpleDateFormat timeView = new SimpleDateFormat("hh:mm aa");
	  SimpleDateFormat date_time_View = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
	  
	  

	public ReminderNameSortListViewAdapter(Context context, ArrayList<ReminderDataPack> objects,String filter_option) {
		super(context,R.layout.reminder_list_item,  objects);
		this.context=context;
		this.objects=objects;
		this.filter_option=filter_option;
		// TODO Auto-generated constructor stub
	}
	
	
	
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    
	    View rowView = inflater.inflate(R.layout.reminder_list_item, parent, false);
	    
	    TextView reminder_list_item_header = (TextView) rowView.findViewById(R.id.reminder_list_item_header);
	    TextView reminder_list_item_schedule_info = (TextView) rowView.findViewById(R.id.reminder_list_item_schedule_info);
	    TextView reminder_list_item_reminder_status = (TextView) rowView.findViewById(R.id.reminder_list_item_reminder_status);
	    TextView reminder_list_item_dosage_size = (TextView) rowView.findViewById(R.id.reminder_list_item_dosage_size);
	    TextView reminder_list_time_list = (TextView) rowView.findViewById(R.id.reminder_list_time_list);
	    ImageView reminder_list_photo = (ImageView) rowView.findViewById(R.id.reminder_list_photo);
	    
	    
	   
	    
	    
	    reminder_list_item_header.setText(filter_option.startsWith(TAG_MEDICINE_NAME_CASE)?objects.get(position).getTaker_name():objects.get(position).getMedicine_name());
	    
	    
	    reminder_list_item_dosage_size.setText(objects.get(position).getQuantity_per_time()+" "+ 
	    context.getResources().getStringArray(R.array.medicineTaketype)[objects.get(position).getTake_form_type_index()]);
	    
	       
	    
	    String msg="";
	    
	    switch (objects.get(position).getSchedule_type_index()) {
		case 0: //Daily
			
			msg="Daily";
			
			break;
			
		case 1: // Day
			
			for(int x=0;x<objects.get(position).getSchedule_data().length();x++){
     		   
     		   if(objects.get(position).getSchedule_data().charAt(x)=='1'){
     			   
     			  msg+=(NewReminderActivity.checkboxesString[x]+ ",");        			   
     		   }
     		  
     	   }
					
			break;
		
		case 2: //Date
			
			msg="Date";
			
			break;
		
			
		case 3: //Custom
			
			int temp_cust_index=Integer.parseInt(objects.get(position).getSchedule_data().substring(0,1));
			
			
			msg="Every "+objects.get(position).getSchedule_data().substring(1)+" "+
			context.getResources().getStringArray(R.array.customType)[temp_cust_index];
			
			break;


		default:
			break;
		}
	    
	    
	    
	    reminder_list_item_schedule_info.setText(msg);
	    
	    reminder_list_photo.setImageURI(Uri.parse(objects.get(position).getPhoto_name()));
	    
	   
		db = new ViewReminder(getContext());
		db.getWritableDatabase();
		
		ArrayList<Long> time_list=db.getDosageTimeList(objects.get(position).getReminder_ld());
		
		
		long temp_current=System.currentTimeMillis();
		
		boolean date_status=objects.get(position).getSchedule_type_index()==3?true:false;  // 3-date
		
		boolean active_status=false;
		db.close();
		
		String time_msg="";
		
		for (Long time : time_list) {
			
			
			if(date_status){
				
				time_msg+=(date_time_View.format(time)+" ");
				if(time>temp_current){
					
					active_status=true;
					
				}
				
			}else {
				
				time_msg+=(timeView.format(time)+" ");
				}
			
			
		}
		
		reminder_list_time_list.setText(time_msg);
		
		
		
		if(objects.get(position).getStart_date()>System.currentTimeMillis() && objects.get(position).getEnd_date()<System.currentTimeMillis() && !date_status){
			
			active_status=true;
			
		}
		
		
		
		if(active_status){
			
			reminder_list_item_reminder_status.setText("Actived");
			reminder_list_item_reminder_status.setTextColor(Color.GREEN);
			
		}else{
			
			reminder_list_item_reminder_status.setText("Expired");
			reminder_list_item_reminder_status.setTextColor(Color.RED);
		}
		
	    

	    return rowView;
	  }
	
	

	

	
	
	

}
