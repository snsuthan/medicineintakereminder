package com.debugers.healthcare.medicineintakereminder;

import java.io.Serializable;

@SuppressWarnings("serial")
public class NotificationDetailsPack implements Serializable  {
	
	String 	taker_feel="";
	
	long 	notification_time;
	
	int 	reminder_status,
			delay_time,
			notification_id;

	ReminderDataPack reminderDataPack;

	public String getTaker_feel() {
		return taker_feel;
	}

	public void setTaker_feel(String taker_feel) {
		this.taker_feel = taker_feel;
	}

	public long getNotification_time() {
		return notification_time;
	}

	public void setNotification_time(long notification_time) {
		this.notification_time = notification_time;
	}

	public int getReminder_status() {
		return reminder_status;
	}

	public void setReminder_status(int reminder_status) {
		this.reminder_status = reminder_status;
	}

	public int getDelay_time() {
		return delay_time;
	}

	public void setDelay_time(int delay_time) {
		this.delay_time = delay_time;
	}

	public int getNotification_id() {
		return notification_id;
	}

	public void setNotification_id(int notification_id) {
		this.notification_id = notification_id;
	}

	public ReminderDataPack getReminderDataPack() {
		return reminderDataPack;
	}

	public void setReminderDataPack(ReminderDataPack reminderDataPack) {
		this.reminderDataPack = reminderDataPack;
	}
	
	
	
	

}
