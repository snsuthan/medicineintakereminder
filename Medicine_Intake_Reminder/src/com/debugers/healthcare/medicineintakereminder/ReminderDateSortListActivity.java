package com.debugers.healthcare.medicineintakereminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;


@SuppressLint("SimpleDateFormat")
public class ReminderDateSortListActivity extends Activity {
	
	
	
	ViewReminder view_reminder;
	
	SimpleDateFormat dateView = new SimpleDateFormat("yyyy-MM-dd");
	
	
	
    ViewFlipper view;
    
	TextView date_sort_date_view;
	
	ListView listview ;
	ArrayList<NotificationDetailsPack> reminder_list;
	
	
	Calendar change_date = Calendar.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.date_sort_view_activity);
		listview = (ListView) findViewById(R.id.date_sort_list_view);
		
		
		date_sort_date_view=(TextView) findViewById(R.id.date_sort_date_view);
		
		ImageView date_sort_left_button=(ImageView) findViewById(R.id.date_sort_left_button);
		ImageView date_sort_right_button=(ImageView) findViewById(R.id.date_sort_right_button);
		
		
		Calendar temp_change_date = Calendar.getInstance();
		temp_change_date.setTimeInMillis(getIntent().getLongExtra("get_date",System.currentTimeMillis()));
		change_date.set(temp_change_date.get(Calendar.YEAR), temp_change_date.get(Calendar.MONTH), temp_change_date.get(Calendar.DAY_OF_MONTH), 0, 0);
		date_sort_date_view.setText(dateView.format(change_date));
		
		
		date_sort_left_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				change_date.add(Calendar.DATE, 1);
				callDataSortRemiderListAdpter();
				
			}
		});
		
		date_sort_right_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				change_date.add(Calendar.DATE, -1);
				callDataSortRemiderListAdpter();
				
			}
		});
		
		
		
		
		
		listview.setOnTouchListener(new OnTouchListener() {
					
			private float lastX,lastY;

			@Override
			public boolean onTouch(View arg0, MotionEvent touchevent) {
				// TODO Auto-generated method stub
				
				switch (touchevent.getAction())
                {
                         
                        case MotionEvent.ACTION_DOWN: 
                        {
                            lastX = touchevent.getX();
                            lastY=touchevent.getY();
                            break;
                       }
                        case MotionEvent.ACTION_UP: 
                        {
                            float currentX = touchevent.getX();
                            float currentY = touchevent.getY();
                            
                            if(Math.abs((currentY-lastY))<60 && Math.abs((currentX-lastX))>250){
                            	
                            //  left to right swipe on screen
                                if (lastX < currentX ) 
                                {
                                	change_date.add(Calendar.DATE, -1);
                    				callDataSortRemiderListAdpter();
                                }
                                
                                // right to left swipe on screen
                                if (lastX > currentX)
                                {
                                	change_date.add(Calendar.DATE, -1);
                    				callDataSortRemiderListAdpter();
                                	
                                }
                            	
                            	
                            }
                            
                            break;
                        }
                }
				return false;
			}
				});
				
				
		
		
		
		
		
		
		
		
		
	}
	
	
	public void callDataSortRemiderListAdpter(){
		
		date_sort_date_view.setText(dateView.format(change_date));
		
		reminder_list=view_reminder.getNotificationList(change_date.getTimeInMillis());
		ReminderDateSortListAdapter listArrayAdapter=new ReminderDateSortListAdapter(getApplicationContext(), reminder_list);
		listview.setAdapter(listArrayAdapter);
		
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		
		super.onResume();
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	

   
  
   
  
	
	

}
