package com.debugers.healthcare.medicineintakereminder;


import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class UpdateReminderActivity extends NewReminderActivity {

	
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//one_reminder_time_list=get_time_list_data.getReminder_time_list();
		Intent get_data=getIntent();
		
		
		
		
		reminderDataPack=(ReminderDataPack) get_data.getSerializableExtra("update_details");
		
		reminder_Id=reminderDataPack.getReminder_ld();
		
		auto_edit_text_view[0].setText(reminderDataPack.getTaker_name());
		auto_edit_text_view[1].setText(reminderDataPack.getMedicine_name());
		
		
		spinerSet(0,R.id.spinner_formOfMedicine,reminderDataPack.getTake_form_type_index(),R.array.takePerDay);
	    per_time_med_take_size_input.setText(reminderDataPack.getQuantity_per_time());
	    
	    
	    spinerSet(1,R.id.spinner_scheduleType,reminderDataPack.getSchedule_type_index(),R.array.scheduleType);
		
		
	   
		
		ImageView photo_of_med=(ImageView) findViewById(R.id.add_photo_of_medicine);
		capturedImageUri=Uri.parse(reminderDataPack.getPhoto_name());
		photo_of_med.setImageURI(capturedImageUri);
		
		
		
		if(!getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Date")){
			
			date_pickers_buttons[0].setText(dateView.format(reminderDataPack.getStart_date()));
			date_pickers_buttons[1].setText(dateView.format(reminderDataPack.getEnd_date()));
			
			
			if(getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Day")){
				
				
				for(int x=0;x<reminderDataPack.getSchedule_data().length();x++){
	        		   
	        		   if(reminderDataPack.getSchedule_data().charAt(x)=='1'){
	        			   
	        			   select_days_text+=(checkboxesString[x]+ ",");        			   
	        		   }
	        		  
	        	   }
				 schedule_set_result_text.setVisibility(View.VISIBLE);
				 schedule_set_result_text.setText(select_days_text);
				
			}else if(getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Custom")){
				
				spinerSet(2,R.id.spinner_customType,reminderDataPack.getSchedule_data().charAt(0),R.array.customType);
				reminder_repeat_count_input.setText(reminderDataPack.getSchedule_data().substring(1));
				
			}
			
			
			}
		
		ViewReminder reminder=new ViewReminder(context);
		
		    NewReminderTimeListAdapter adapter = new NewReminderTimeListAdapter(this,reminder.getDosageTimeList(reminder_Id),date_status);
	    	dosage_time_list_view.setAdapter(adapter);
		
		
		
	}
	
	
	public void spinerSet(int sppinerNo,int spinner_id,int position,int array_id){
		spinner[sppinerNo].setSelection(position);
		setScheduleTypeSelection(spinner_id, getApplicationContext().getResources().getStringArray(array_id)[position], position);
		
		
	}
	

	

}
