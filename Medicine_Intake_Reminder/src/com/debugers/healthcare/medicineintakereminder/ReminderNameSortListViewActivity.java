package com.debugers.healthcare.medicineintakereminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.debugers.healthcare.medicineintakereminder.database.DeleteReminder;
import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class ReminderNameSortListViewActivity extends Activity {
	
	
	public final static String TAG_FILTER_OPTION="filter";

final Context context = this;	


SimpleDateFormat timeView = new SimpleDateFormat("hh:mm aa");
SimpleDateFormat dateView = new SimpleDateFormat("yyyy-MM-dd");
	 
	
	public ArrayList<ReminderDataPack> reminder_list = new ArrayList<ReminderDataPack>();
	ViewReminder viewReminder;
	DeleteReminder deleteReminder;
	
	String filter_option;
	
	
	ReminderNameSortListViewAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_list_activity);
		
		 getActionBar().setDisplayHomeAsUpEnabled(true);
		
		final ListView listview = (ListView) findViewById(R.id.reminder_list_view);
		
		viewReminder=new ViewReminder(context);
		
		filter_option=getIntent().getStringExtra(TAG_FILTER_OPTION);
		
		reminder_list=viewReminder.getReminderList(filter_option);
		
		

        
	adapter = new ReminderNameSortListViewAdapter(this, reminder_list,getIntent().getStringExtra(TAG_FILTER_OPTION));
    listview.setAdapter(adapter);

      listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent,  View view,
            int position, long id) {
        	final int reminder_possion=position;
        	
        	
        	final Dialog dialog = new Dialog(ReminderNameSortListViewActivity.this);
        	dialog.setTitle("Reminder View");
        	dialog.setContentView(R.layout.one_reminder_view_dialog);
        	
        
        	TextView one_reminder_dialog_taker_name=(TextView) dialog.findViewById(R.id.one_reminder_dialog_taker_name);
        	TextView one_reminder_dialog_medicine_name=(TextView) dialog.findViewById(R.id.one_reminder_dialog_medicine_name);
        	
        	TextView one_reminder_dialog_end_date=(TextView) dialog.findViewById(R.id.one_reminder_dialog_end_date);
        	TextView one_reminder_dialog_start_date=(TextView) dialog.findViewById(R.id.one_reminder_dialog_start_date);
        	
        	TextView one_reminder_dialog_quantity_per_one_time=(TextView) dialog.findViewById(R.id.one_reminder_dialog_quantity_per_one_time);
        	
        	TextView one_reminder_dialog_shedule_data_text=(TextView) dialog.findViewById(R.id.one_reminder_dialog_shedule_data_text);
        	TextView one_reminder_dialog_dosage_time_set=(TextView) dialog.findViewById(R.id.one_reminder_dialog_dosage_time_set);
        	
        	ImageView dialog_photo=(ImageView) dialog.findViewById(R.id.photo_of_medicine);
        	
        	Button dialog_reminder_cancel_button=(Button) dialog.findViewById(R.id.one_reminder_dialog_cancel_button);
        	Button dialog_reminder_delete_button=(Button) dialog.findViewById(R.id.one_reminder_dialog_delete_button);
        	Button dialog_reminder_update_button=(Button) dialog.findViewById(R.id.one_reminder_dialog_update_button);
        	
        	
        	TextView info_text = (TextView) view.findViewById(R.id.reminder_list_item_schedule_info); 
        	TextView time_text = (TextView) view.findViewById(R.id.reminder_list_time_list);
        	
        	one_reminder_dialog_shedule_data_text.setText( time_text.getText());
        	one_reminder_dialog_dosage_time_set.setText( info_text.getText());
        	
        	
        	one_reminder_dialog_taker_name.setText(reminder_list.get(reminder_possion).getTaker_name());
        	one_reminder_dialog_medicine_name.setText(reminder_list.get(reminder_possion).getMedicine_name());
        	
        	dialog.findViewById(R.id.date_sort_start_end_date_layout).setVisibility(View.GONE);
        	
        	if(reminder_list.get(reminder_possion).getSchedule_type_index()!=3){
        		
        		dialog.findViewById(R.id.date_sort_start_end_date_layout).setVisibility(View.VISIBLE);
        		
        		one_reminder_dialog_end_date.setText(dateView.format(reminder_list.get(reminder_possion).getEnd_date()));
            	one_reminder_dialog_start_date.setText(dateView.format(reminder_list.get(reminder_possion).getStart_date()));
        		
        	}
        	
        	
        	
        	
        	one_reminder_dialog_quantity_per_one_time.setText(reminder_list.get(reminder_possion).getQuantity_per_time()+" "+ 
        			context.getResources().getStringArray(R.array.scheduleType)[reminder_list.get(reminder_possion).getTake_form_type_index()]);
        	
        	
        	
        	
        	
        	dialog_photo.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                	 medicinePhotoView(reminder_list.get(reminder_possion).getPhoto_name());}
                });
        	
        	
        	
        	
        	dialog_reminder_cancel_button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                	dialog.cancel();
                	
                	 }
                });
        	
        	dialog_reminder_delete_button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                	dialog.cancel();
                	deleteReminder(reminder_list.get(reminder_possion).getReminder_ld(),reminder_possion);
                	
					
                	
                	 }
                });
        	dialog_reminder_update_button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                    	
                    	Intent updateView=new Intent(context,UpdateReminderActivity.class);
                    	updateView.putExtra("update_details",reminder_list.get(reminder_possion));
                    	startActivity(updateView);
                    	
                    	
                    	 }
                    });
        	
        	
        	
        	dialog.show();
        	
        	
        	
        }

      });
      
      listview.setOnItemLongClickListener(new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int reminder_possion, long arg3) {
			
			// TODO Auto-generated method stub
			deleteReminder(reminder_list.get(reminder_possion).getReminder_ld(),reminder_possion);
			
			//adapter.notifyDataSetChanged();
			return false;
		}
	});
	
		
        
        
        
    }

	@Override
	protected void onResume() {
		
		// TODO Auto-generated method stub
		
				super.onResume();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	
	public void medicinePhotoView(String photo_name){
		final Dialog photo_dialog = new Dialog(ReminderNameSortListViewActivity.this);
		photo_dialog.setTitle("Medicine Photo View");
		photo_dialog.setContentView(R.layout.dialog_medicine_photo_full_view);
		
    	ImageView dialog_medicine_full_view_photo=(ImageView) photo_dialog.findViewById(R.id.dialog_medicine_full_view_photo);
    	Button dialog_full_view_btn =(Button) photo_dialog.findViewById(R.id.dialog_full_view_btn);
    	dialog_medicine_full_view_photo.setImageURI(Uri.parse(photo_name));
    	
    	dialog_full_view_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	
            	photo_dialog.cancel();
               


            }
        });
    	
    	
    	photo_dialog.show();
		
	}
	public void deleteReminder(final long reminder_id,final int reminder_possion){
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
	 
	
			alertDialogBuilder.setTitle("Delete Reminder");
 
			// set dialog message
			alertDialogBuilder
			
				.setMessage("Are you sure you want to delete?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						deleteReminder = new DeleteReminder(getApplicationContext());
						deleteReminder.deleteReminder(reminder_id,reminder_list.get(reminder_possion).getPhoto_name());
						reminder_list.remove(reminder_possion);
						adapter.notifyDataSetChanged();
						dialog.cancel();
						
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
		
		
	}

	
	
	

		
}
	
    
   
    
    
    
    
    
    
    
    
    
    
