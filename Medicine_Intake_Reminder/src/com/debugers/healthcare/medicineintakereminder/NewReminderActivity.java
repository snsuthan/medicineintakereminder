package com.debugers.healthcare.medicineintakereminder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;













import com.debugers.healthcare.medicineintakereminder.database.AddReminder;
import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.Long2;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

@SuppressLint("SimpleDateFormat")
public class NewReminderActivity extends Activity implements TextWatcher,OnItemSelectedListener{
	
	
	
	final static String TAG_NOTIFACATION_TIME="time";
	final static String TAG_NOTIFACATION_ID="notification_id";
	final static String TAG_REMINDER_ID="reminder_id";
	
	
	
	public final static String TAG_TAKER_NAME_CASE="taker_name";
	public final static String TAG_MEDICINE_NAME_CASE="medicine_name";
	
	
	List<String> take_name_list,medicine_name_list;
	
	
	ArrayList<HashMap<String, Long>> notification_list=new ArrayList<HashMap<String, Long>>();
	ArrayList<Long> time_schedule_list;
	
	ArrayAdapter<String> myAutoCompleteAdapter;
	NewReminderTimeListAdapter time_adapter;
	
	Calendar calendar= Calendar.getInstance();
	File file;
	Uri capturedImageUri;
	
	
	SimpleDateFormat dateView = new SimpleDateFormat("yyyy-MM-dd");
	
	 Calendar notifation_time=Calendar.getInstance();
	
	final Context context = this;
	
	long reminder_Id,photo_name;
	
	int repeat_custom_type_index;
	
	
	
    AutoCompleteTextView[] auto_edit_text_view =new AutoCompleteTextView[2];
    Spinner[]  spinner= new Spinner[3];
    Button[]    date_pickers_buttons= new Button[2];
   
    EditText reminder_repeat_count_input,per_time_med_take_size_input;
  
    
    public static String[]     checkboxesString={"Sunday","Monday","Tuesday","Wedneday","Thursday","Friday","Saturday"};
    
    
    
    String select_days_text="";
    
    
    ReminderDataPack reminderDataPack;
    
    ListView dosage_time_list_view;
    
    ImageView add_photo_of_medicine;
    
    
    int[]   autoEditTextId={R.id.text_medicineTaker,R.id.text_medicineName},
    	   // editTextId={R.id.text_noteOfMedicine,R.id.text_numberOfSchedule,R.id.sameSizeOfMedicineText,R.id.breakfastSize,R.id.lunchSize,R.id.dinnerSize},
    	    spinnerId={R.id.spinner_formOfMedicine,R.id.spinner_scheduleType,R.id.spinner_customType},
    	    spinnerArrayListId={R.array.medicineTaketype,R.array.scheduleType,R.array.customType},
            datePickersButtonsId={R.id.startDayButton,R.id.endDayButton}/*,
    		checkboxesId={R.id.CheckSun,R.id.checkMon,R.id.CheckTue,R.id.CheckWed,R.id.CheckThu,R.id.checkFri,R.id.CheckSat,R.id.breakfast,R.id.lunch,R.id.dinner}*/;
	
        	

    TextView schedule_type_name,dosage_text,schedule_set_result_text;


	public boolean date_status=false;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		
		
		
		photo_name=calendar.getTimeInMillis();
		
		capturedImageUri= Uri.parse(getResources().getDrawable(R.drawable.ic_launcher).toString());
		
		dosage_time_list_view=(ListView) findViewById(R.id.dosage_time_list_view);
		
		
		 add_photo_of_medicine=(ImageView) findViewById(R.id.add_photo_of_medicine);
		
		
		schedule_type_name=(TextView)findViewById(R.id.schedule_type_name);
		dosage_text=(TextView)findViewById(R.id.dosage_text);
		
		schedule_set_result_text=(TextView)findViewById(R.id.schedule_set_result_text);
		
		per_time_med_take_size_input=(EditText)findViewById(R.id.per_time_med_take_size_input);
		reminder_repeat_count_input=(EditText)findViewById(R.id.reminder_repeat_count_input);
		
		time_schedule_list =new ArrayList<Long>();
		
		reminderDataPack=new ReminderDataPack();
		
		
		add_photo_of_medicine.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				onCamaraButtonClick(v);
				
			}
		});
		
		
		time_adapter= new NewReminderTimeListAdapter(getBaseContext(),time_schedule_list,date_status);
    	dosage_time_list_view.setAdapter(time_adapter);
		
		
		
		
		for(int x=0;x<3;x++){
			spinner[x]=(Spinner) findViewById(spinnerId[x]);
			spElements(x);
			
			switch (x) {
			case 0:
				
				reminderDataPack.setTake_form_type_index(0);
				break;
				
			case 1:
				
				reminderDataPack.setSchedule_type_index(0);
				
				break;
			case 2:
				
				repeat_custom_type_index=0;
	
				break;

			default:
				break;
			}
			
			if(x<2){
				auto_edit_text_view[x]=(AutoCompleteTextView) findViewById(autoEditTextId[x]);
				prepareMyList(x);
				date_pickers_buttons[x]=(Button) findViewById(datePickersButtonsId[x]);
				}
			
		}
				
			
			
			
			
			
	}
	
	
	
	
	
	
	// ************  AutoCompleteTextView *************** //
	  
	  
	  
    void prepareMyList(int x){
  	     //prepare your list of words for AutoComplete
  	     AutoCompleteTextView myAutoComplete=auto_edit_text_view[x] ;
  	     
  	     ViewReminder db=new ViewReminder(getBaseContext());
  	     db.getWritableDatabase();
  	     
  	     String filter_option;
  	     
  	     if(x==0){
  	    	 
  	    	filter_option=TAG_TAKER_NAME_CASE;
  	    	 take_name_list=new ArrayList<String>();
  	    	 
  	     }else{
  	    	 
  	    	filter_option=TAG_MEDICINE_NAME_CASE;
  	    	medicine_name_list=new ArrayList<String>();
  	    	 
  	     }
  	     
		 ArrayList<ReminderDataPack> reminder_list = db.getReminderFilterList(filter_option);
		 
		
		
			for (int i = 0; i < reminder_list.size(); i++) {
				
				if(x==0){ take_name_list.add(reminder_list.get(i).getTaker_name());

				}else{medicine_name_list.add(reminder_list.get(i).getMedicine_name());}
					
			}
		
		
		db.close();
  	     
  	     myAutoComplete.addTextChangedListener(this);
       
              myAutoCompleteAdapter = new ArrayAdapter<String>(
    		  getBaseContext(), R.layout.spinner_dropdown_item,x==0?take_name_list:medicine_name_list);
      
       myAutoComplete.setAdapter(myAutoCompleteAdapter);
       
  }
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	// ************  AutoCompleteTextView *************** //
	
	// ************  Spinner *************** //
	

	public void spElements(int x) {
		Spinner sp = spinner[x];
		sp.setOnItemSelectedListener(this);
		ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(getBaseContext(),
		      spinnerArrayListId[x], R.layout.spinner_item);

		// Drop down layout style - list view with radio button
		dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

		// attaching data adapter to spinner
		sp.setAdapter(dataAdapter);
		
	}

	@Override
	public void onItemSelected(AdapterView<?> main, View view, int position,
			long Id) {
		
		// Check which Item Selected
		
		String item = main.getItemAtPosition(position).toString();
		Spinner spinner = (Spinner) main;
		
		setScheduleTypeSelection(spinner.getId(),item,position);
		
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
		

	}

	
	
	
	 public void chooseDate(final View v) {

		 
		 final Calendar dateAndTime=Calendar.getInstance();
		 DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
			    
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			    
			      
			      dateAndTime.set(year, monthOfYear, dayOfMonth);
			    
			      
			     
			     long date=dateAndTime.getTimeInMillis();
			     // Check which Date Picker was set
			      if(v.getId()==R.id.startDayButton){
			    	  reminderDataPack.setStart_date(date);
			    	  date_pickers_buttons[0].setText(dateView.format(date));
			    	  
			      }else {
			    	  
			    	  reminderDataPack.setEnd_date(date);
			    	  date_pickers_buttons[1].setText(dateView.format(date));
			    	  
			    	  }
			     
			   
			    
				
				}
			  };

		  
	    new DatePickerDialog(NewReminderActivity.this, d,dateAndTime.get(Calendar.YEAR),dateAndTime.get(Calendar.MONTH),dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
	    
	   
	    
	    
	    
	  }
	 
	 
	 public void setScheduleTypeSelection(int id,String item,int position){
		 
		 
		 switch (id) {
			case R.id.spinner_scheduleType:
				
				
				 reminderDataPack.setSchedule_type_index(position);
					
					findViewById(R.id.schedule_Layout).setVisibility(View.GONE);
					findViewById(R.id.customScheduleAction).setVisibility(View.GONE);
					schedule_set_result_text.setVisibility(View.GONE);
					
					time_schedule_list.clear();
					time_adapter.notifyDataSetChanged();
					
					String schedule_type_text = "",dosage_type_text="";
					
					
		         if(item.equals("Date")){  
						
						schedule_type_text="Date Schedule";
						dosage_type_text="Dosage Date and Time";
						
						date_status=true;
						
						
						
					}else{
						
						date_status=false;
						String temp_st_date="",temp_ed_date="";
					  
						
						dosage_type_text="Dosage Time";  
						    
						
						 long temp_time=System.currentTimeMillis();
				    	  
				    	  reminderDataPack.setStart_date(temp_time);
				    	  reminderDataPack.setEnd_date(temp_time);
					
				    	  temp_ed_date=temp_st_date=dateView.format(new Date(temp_time));
						
						
						date_pickers_buttons[0].setText(temp_st_date);
						date_pickers_buttons[1].setText(temp_ed_date);
						
						
						 findViewById(R.id.schedule_Layout).setVisibility(View.VISIBLE);
						
						
						
						 if(item.equals("Daily")){  // Daily
			                  //Layout daily action visibility
							
							 
							 schedule_type_text="Daily Schedule";
			
			
		              }else if(item.equals("Day")){  // Day
			                  //Layout visibility
		             	 
		             	 setDaySchedule();
		             	 
		             	 
		             	 schedule_type_text="Day Schedule";
		             	 
			
			
		             }else if(item.equals("Custom")){  // Custom
			                  //Layout visibility
		             	schedule_type_text="Custom Schedule";
		             	
		             	findViewById(R.id.customScheduleAction).setVisibility(View.VISIBLE);
		             	
		             	
		             	 
			
		              }
						
						
						
						            
					}
						 
						
		         
		         schedule_type_name.setText(schedule_type_text);
					dosage_text.setText(dosage_type_text);
				
				          
				
				 break;
				
	            
		
	             case R.id.spinner_formOfMedicine:
	            	 reminderDataPack.setTake_form_type_index(position);
		
		         break;
		         
		         
	             case R.id.spinner_customType:
	            	 
	            	 repeat_custom_type_index=position;
		
		         break;
		
	          

			default:
				break;
			}
		 
		 
		 
		
	 }

	 
	 
	public void setDaySchedule(){
		
		
		
		final StringBuilder select_days = new StringBuilder("0000000");
		
		
		
		  AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setTitle("Select Days");
          builder.setMultiChoiceItems(checkboxesString, null,
                  new DialogInterface.OnMultiChoiceClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int indexSelected,
                   boolean isChecked) {
               if (isChecked) {
                   // If the user checked the item, add it to the selected items
            	    
            	   select_days.setCharAt(indexSelected, '1');
                  
               } else{
            	   
            	   select_days.setCharAt(indexSelected, '0'); 
               } 
           }
       })
        // Set the action buttons
       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int id) {
              
        	   reminderDataPack.setSchedule_data(select_days.toString());
        	   
        	   for(int x=0;x<select_days.length();x++){
        		   
        		   if(select_days.charAt(x)=='1'){
        			   
        			   select_days_text+=(checkboxesString[x]+ ",");        			   
        		   }
        		  
        	   }
        	   
        	   
        	     schedule_set_result_text.setVisibility(View.VISIBLE);
            	 schedule_set_result_text.setText(select_days_text);

           }
       });
          
          
         

          AlertDialog dialog = builder.create();
          dialog.show();
     }
	
	
	
	
	public void addDosageTime(View view){
		
		
		if(!date_status){
			

			TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
				   
				public void onTimeSet(TimePicker view, int hourOfDay,
			                          int minute) {
					
					
			      calendar.set(hourOfDay,minute);
			   
				     long time=calendar.getTimeInMillis();
				    
				     time_schedule_list.add(time);
				     time_adapter.notifyDataSetChanged();
				     
				     }
				    
				     
			      };  
	         new TimePickerDialog(context, t,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),true).show();
			
			
			
			
		
			
		}else{
			
			time_schedule_list.add(System.currentTimeMillis());
			time_adapter.notifyDataSetChanged();
			
		      }
	     
	     
		
		
	}
	
	
	public int  setNotificationDataPack(long time,int notification_Id){
		
		
		 HashMap<String, Long> notification = new HashMap<String, Long>();
		 notification.put(TAG_NOTIFACATION_ID,Long.valueOf(notification_Id));
		 notification.put(TAG_NOTIFACATION_TIME,time);
		 notification.put(TAG_REMINDER_ID,reminder_Id);
	     
	     notification_list.add(notification);
	     
		
		return notification_Id;
		
	}
	

	
	
	
	  
	  public void notificationHandled(int notification_Id){
			
			
			
			if(date_status){  //Date
				
				for(int x=0;x<time_schedule_list.size();x++){
						
						long time=time_schedule_list.get(x);
						
						Intent intent = new Intent(getBaseContext(),NotificationDisplay.class);
						
						intent.putExtra(TAG_NOTIFACATION_ID, setNotificationDataPack(time,notification_Id));
						
						PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),notification_Id, intent, 0);
						AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
						
						alarmManager.set(AlarmManager.RTC_WAKEUP,time, pendingIntent);
						
						
						notification_Id++;
					   }
					
					
					
				
				
				
				
				 
	   
				
				
			}else if (getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Custom") ||
					getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Daily")){   
				
				int date_intervel=1;
				
				
				
				if(getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Custom")){
					
					
					if(repeat_custom_type_index==0){ // Day
						date_intervel=1;
						
					}else if(repeat_custom_type_index==1){ // Week
						
						date_intervel=7;
						
					}else{   // Month
						date_intervel=30;
						
					}
					
					date_intervel*=Integer.parseInt(reminderDataPack.getSchedule_data().substring(1));  // 1 Number The Custom Type Index
					
				}
				
				
					
					Calendar date=Calendar.getInstance();
					date.setTimeInMillis(reminderDataPack.getStart_date());
					
					
				
				
				
                 for(int x=0;x<time_schedule_list.size();x++){
					    
                	    
                	    Calendar time=Calendar.getInstance();
    					time.setTimeInMillis(time_schedule_list.get(x));
    					
    					notifation_time.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
                	    
						
						Intent intent = new Intent(getBaseContext(),NotificationDisplay.class);
						
						intent.putExtra(TAG_NOTIFACATION_ID, setNotificationDataPack(notifation_time.getTimeInMillis(),notification_Id));
						
						PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),notification_Id, intent, 0);
						AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
						alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, notifation_time.getTimeInMillis(),AlarmManager.INTERVAL_DAY*date_intervel, pendingIntent);
						
						notification_Id++;
					}
                 
                 
                 
                 
                 
					   
			} else if((getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Day"))){
				
				Calendar date=Calendar.getInstance();
				date.setTimeInMillis(reminderDataPack.getStart_date());
				
				
				int getDayOfWeek=date.get(Calendar.DAY_OF_WEEK);
				
				String temp_select_days=reminderDataPack.getSchedule_data();
				
				
				for(int day_of_week=1;day_of_week<=temp_select_days.length();day_of_week++){
					
					
					if(temp_select_days.indexOf(day_of_week)=='1'){  // 1 -means Day Checked
						
						long time_interval=(day_of_week-getDayOfWeek)>=0?(day_of_week-getDayOfWeek):(7-Math.abs(day_of_week-getDayOfWeek));
						
						time_interval=time_interval*AlarmManager.INTERVAL_DAY;
						
						for(int y=0;y<time_schedule_list.size();y++){
							
							
							    Calendar time=Calendar.getInstance();
		    					time.setTimeInMillis(time_schedule_list.get(y));
		    					
		    					notifation_time.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
		                	    
							
		    					notifation_time.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
		                	    
								
								Intent intent = new Intent(getBaseContext(),NotificationDisplay.class);
								
								intent.putExtra(TAG_NOTIFACATION_ID, setNotificationDataPack(notifation_time.getTimeInMillis()+time_interval,notification_Id));
								
								PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),notification_Id, intent, 0);
								AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
								alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, notifation_time.getTimeInMillis()+time_interval,AlarmManager.INTERVAL_DAY*7, pendingIntent);
		    					
								notification_Id++;
							  
							
							
						   }
						}
						
						
						
					}
				}
				
				
				
			
			
			if(!date_status){
				
				Calendar end_date=Calendar.getInstance();
				end_date.setTimeInMillis(reminderDataPack.getEnd_date());
				
				notifation_time.set(end_date.get(Calendar.YEAR), end_date.get(Calendar.MONTH), end_date.get(Calendar.DAY_OF_MONTH),23,59);
				
				
				Intent intent = new Intent(getBaseContext(),NotificationRemove.class);
				intent.putExtra(TAG_REMINDER_ID,reminder_Id);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),notification_Id+1000, intent,PendingIntent.FLAG_UPDATE_CURRENT);
				AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
				alarmManager.set(AlarmManager.RTC_WAKEUP,notifation_time.getTimeInMillis(), pendingIntent);
				
				
				
			}
			
		
			
		}
	  

	  
	  public boolean  isAllFieldFilled(){
		  
		  
		 String  meassge_text="";
		 
		 boolean result=false;
		  
		  if(auto_edit_text_view[0].getText().toString().equals("")){
			  
			  meassge_text="Please enter Patient Name";
			  auto_edit_text_view[0].requestFocus();
		  
		  }else if(auto_edit_text_view[1].getText().toString().equals("")){
			  
			  meassge_text="Please enter Medicine name";
			  auto_edit_text_view[1].requestFocus();
			  
			  
		  }else if(per_time_med_take_size_input.getText().toString().equals("")){
			  
			    meassge_text="add value to quantity per time";
			    per_time_med_take_size_input.requestFocus();
			    
		  }else if(!date_status && reminderDataPack.getEnd_date()<reminderDataPack.getStart_date()){
			  
			  meassge_text="Change End Date Biger than Start Date";
    		  chooseDate(findViewById(R.id.endDayButton));
			  
			 
			  
			  
		  }else if(getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Custom") &&
				  reminder_repeat_count_input.getText().toString().equals("") ){
			  

			    meassge_text="add values to repeat schedule";
				reminder_repeat_count_input.requestFocus();
			  
			  
			  
			  
		  }else if(time_schedule_list.isEmpty()){
					
					meassge_text="Please add atlest one Dosage Time";
					
		  }else{
					
					result=true;
					
				}
		  
		  if(!meassge_text.equals("")){
			  Toast.makeText(getBaseContext(), meassge_text,Toast.LENGTH_LONG).show();
			  
		  }
		  
		  
		  
		  
		  return result;
		  
		  
	  }
	  

	
	public void onClickSaveButton(View v) {
		// TODO Auto-generated method stub
		
		
			     
		if(isAllFieldFilled()) {
			
				reminderDataPack.setTaker_name(auto_edit_text_view[0].getText().toString());
				reminderDataPack.setMedicine_name(auto_edit_text_view[1].getText().toString());
				reminderDataPack.setPhoto_name(capturedImageUri.toString());
				
				
				
				if(getBaseContext().getResources().getStringArray(R.array.scheduleType)[reminderDataPack.getSchedule_type_index()].equals("Custom")){
					
					reminderDataPack.setSchedule_data(repeat_custom_type_index+reminder_repeat_count_input.getText().toString());
				}
				
				reminderDataPack.setQuantity_per_time(Integer.parseInt(per_time_med_take_size_input.getText().toString()));
				
			
			
				reminder_Id=System.currentTimeMillis();
				
				reminderDataPack.setReminder_ld(reminder_Id);
				
				
				
				notificationHandled((int) (reminder_Id/1000));	
				
				new AddReminder(getBaseContext(),notification_list,time_schedule_list,reminderDataPack);
				
				notification_list.clear();
				reminderDataPack=new ReminderDataPack();
				photo_name=calendar.getTimeInMillis();
				capturedImageUri= Uri.parse(getResources().getDrawable(R.drawable.ic_launcher).toString());
				Toast.makeText(getBaseContext(), "Reminder Create Successfully",Toast.LENGTH_LONG).show();
				
				
			}
				
				

	}
	
	
	public void resetButtonOnClick(View v){
		
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		
		alertDialogBuilder.setTitle("Reset!")
				.setMessage("Are you Sure want reset Reminder ?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								
								
								capturedImageUri= Uri.parse(getResources().getDrawable(R.drawable.ic_launcher).toString());
								
								reminder_repeat_count_input.getEditableText().clear();
								per_time_med_take_size_input.getEditableText().clear();
								
								
						    	time_schedule_list.clear();
								time_adapter.notifyDataSetChanged();
								
								
								reminderDataPack=new ReminderDataPack();
								
								
								for(int x=0;x<3;x++){
									spinner[x].setSelection(0);
									
									if(x<2){
										
										auto_edit_text_view[x].getEditableText().clear();
										date_pickers_buttons[x].setText(dateView.format(new Date(System.currentTimeMillis())));
										
										}
									
									
								}
								
								
							      
								
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
		
	}
	
	public void onCamaraButtonClick(View v){
		
		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/Android/data/com.debugers.healthcare.medicineintakereminder");    
		myDir.mkdirs();
		file = new File(myDir,"reminder_"+photo_name+".jpg");
	    if(!file.exists()){
	       try {
	    	
	            file.createNewFile();
	       } catch (IOException e) {
	           // TODO Auto-generated catch block
	            e.printStackTrace();
	       }
	          }else{
	               file.delete();
	       try {
	               file.createNewFile();
	       } catch (IOException e) {
	              // TODO Auto-generated catch block
	              e.printStackTrace();
	       }
	    }
	    capturedImageUri = Uri.fromFile(file);
	    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	    i.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
	    
		startActivityForResult(i,0);
    }
	    

	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
		
	    if (requestCode ==0) {  
	        
	        try {
	        	
	    Bitmap bitmap = MediaStore.Images.Media.getBitmap( getApplicationContext().getContentResolver(),capturedImageUri);
	    FileOutputStream out = new FileOutputStream(file);
	      bitmap= Bitmap.createScaledBitmap(bitmap, 400, 400,true);
	      bitmap.compress(CompressFormat.JPEG,90,out);
	      bitmap= Bitmap.createScaledBitmap(bitmap, 100, 100,true);
	      
	   
	    
	     add_photo_of_medicine.setImageBitmap(bitmap );
	    
	    
	          } catch (FileNotFoundException e) {
	              // TODO Auto-generated catch block
	              e.printStackTrace();
	          } catch (IOException e) {
	               // TODO Auto-generated catch block
	               e.printStackTrace();
	         }
	    }  
	}
	
	
	
	
	
	
	

	
}
