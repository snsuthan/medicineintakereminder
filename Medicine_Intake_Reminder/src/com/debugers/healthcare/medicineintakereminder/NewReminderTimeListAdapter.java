package com.debugers.healthcare.medicineintakereminder;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;




import java.util.Date;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

@SuppressLint("SimpleDateFormat")
public class NewReminderTimeListAdapter extends BaseAdapter{

	Context context;
	ArrayList<Long> time_schedule_list;
	
	
	
	SimpleDateFormat timeView = new SimpleDateFormat("hh:mm aa");
	SimpleDateFormat dateView = new SimpleDateFormat("yyyy-MM-dd");
	Calendar dateAndTime=Calendar.getInstance();
	String take_size;
	
	
	int list_position;
	
	boolean date_status;
	
	
	int year,monthOfYear,dayOfMonth,hourOfDay,minute;
	
	public NewReminderTimeListAdapter(Context context,ArrayList<Long> time_schedule_list,boolean date_status) {
		
		
		this.context=context;
		this.time_schedule_list=time_schedule_list;
		this.date_status=date_status;
		// TODO Auto-generated constructor stub
		
		year=dateAndTime.get(Calendar.YEAR);
		monthOfYear=dateAndTime.get(Calendar.MONTH);
		dayOfMonth=dateAndTime.get(Calendar.DAY_OF_MONTH);
		hourOfDay=dateAndTime.get(Calendar.HOUR_OF_DAY);
		minute=dateAndTime.get(Calendar.MINUTE);
		
	}

	

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	   
	    list_position=position;
	    
	    if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.dateandtime_list_item, null);
		}
	   
	    
	    final Button date_Button = (Button) convertView.findViewById(R.id.date_Button);
	    final Button time_Button = (Button) convertView.findViewById(R.id.time_Button);
	    
	    TextView dosgae_time_item_text= (TextView)convertView.findViewById(R.id.dosgae_time_item_text);
	    
	    ImageView remove_Button = (ImageView) convertView.findViewById(R.id.time_remove_image);
	    
	    
	    convertView.findViewById(R.id.date_schedule_layout).setVisibility(View.GONE);
	    convertView.findViewById(R.id.time_schedule_layout).setVisibility(View.GONE);

	    
	    time_Button.setText(timeView.format(time_schedule_list.get(list_position)));
	    
	    if(date_status){
	    	
	    	
	    	convertView.findViewById(R.id.date_schedule_layout).setVisibility(View.VISIBLE);
	    	date_Button.setText(dateView.format(time_schedule_list.get(list_position)));
	    }else{
	    	
	    	 convertView.findViewById(R.id.time_schedule_layout).setVisibility(View.VISIBLE);
	    	 dosgae_time_item_text.setText("Set Time : "+timeView.format(time_schedule_list.get(list_position)));
	    	
	    }
		
	    
	    
	   
	   
	    remove_Button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				time_schedule_list.remove(list_position);
				notifyDataSetChanged();
				
				
			}
		});
		
	      
	   
	  
	    
	    date_Button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				DatePickerDialog.OnDateSetListener d=new DatePickerDialog.OnDateSetListener() {
				    
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						
					dateAndTime.set(year, monthOfYear, dayOfMonth,hourOfDay,minute);
				    long date=dateAndTime.getTimeInMillis();
				     
				    
				     
				     time_schedule_list.set(list_position,date); 
				     
				     date_Button.setText(dateView.format(date));
				     
			  
				    	  
				    	  
					}	  
				};
				new DatePickerDialog(context, d,dateAndTime.get(Calendar.YEAR),dateAndTime.get(Calendar.MONTH),dateAndTime.get(Calendar.DAY_OF_MONTH)).show();
				
			}
			
			
	  });
	    
	    
       time_Button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				TimePickerDialog.OnTimeSetListener t=new TimePickerDialog.OnTimeSetListener() {
					   
					public void onTimeSet(TimePicker view, int hourOfDay,
				                          int minute) {
				      dateAndTime.set(year, monthOfYear, dayOfMonth,hourOfDay,minute);
				      
				   
					     long time=dateAndTime.getTimeInMillis();
					    
					     time_schedule_list.set(list_position,time); 
					     
					     time_Button.setText(timeView.format(time));
					     

				        
					     }
					    
					     
				      };  
		         new TimePickerDialog(context, t,dateAndTime.get(Calendar.HOUR_OF_DAY),dateAndTime.get(Calendar.MINUTE),true).show();
				
				
				
				
			}
		});
	    
	    
	    
	    
	   

	    return convertView;
	  
	}




	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return time_schedule_list.size();
	}




	@Override
	public Long getItem(int position) {
		// TODO Auto-generated method stub
		return time_schedule_list.get(position);
	}




	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	} 
	
	

}

