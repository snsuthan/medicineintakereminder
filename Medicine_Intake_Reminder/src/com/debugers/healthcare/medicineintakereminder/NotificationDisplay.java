package com.debugers.healthcare.medicineintakereminder;

import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;


public class NotificationDisplay extends BroadcastReceiver {
	
	final static String TAG_NOTIFACATION_ID="notification_id";
	
	
	int notification_id;
	@Override
	public void onReceive(Context context, Intent intent) {
		
		
		notification_id=intent.getIntExtra(TAG_NOTIFACATION_ID, 0);
		
		
        Intent notification = new Intent(context, NotificationReceiverActivity.class);
        notification.putExtra(TAG_NOTIFACATION_ID,0);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, notification, PendingIntent.FLAG_CANCEL_CURRENT);
		
		
		
		ViewReminder reminder=new ViewReminder(context);
		NotificationDetailsPack detailsPack=reminder.getNotificationReminderDetails(notification_id);
		ReminderDataPack dataPack=detailsPack.getReminderDataPack();
		NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(context)
		
		.setSmallIcon(R.drawable.ic_launcher)
		.setContentIntent(pIntent)
		.setContentTitle("It's time to take Medicine")
		.setContentText(dataPack.getMedicine_name()).setSubText("Size :"+detailsPack.getReminderDataPack().getQuantity_per_time()+
				" "+  context.getResources().getStringArray(R.id.spinner_formOfMedicine)[dataPack.getSchedule_type_index()]).setSubText("Time :"+
						detailsPack.getNotification_time()).setAutoCancel(true)
		.setContentInfo("for "+dataPack.getTaker_name())
		.setDefaults(Notification.DEFAULT_SOUND);
		
		
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		mNotificationManager.notify(1001, mBuilder.build());
		
		
		
		
		
		
		
		
		
		 

	}

}
