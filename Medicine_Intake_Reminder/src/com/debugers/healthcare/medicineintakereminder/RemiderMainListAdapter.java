package com.debugers.healthcare.medicineintakereminder;

import java.util.ArrayList;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RemiderMainListAdapter extends ArrayAdapter<ReminderDataPack>{
	
	
	ArrayList<ReminderDataPack> reminder_filter_list;
	
	
	Context context;

	public RemiderMainListAdapter(Context context,ArrayList<ReminderDataPack> reminder_filter_list) {
		super(context,R.layout.reminder_main_list_item,reminder_filter_list);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.reminder_filter_list=reminder_filter_list;
	}
	
	
	static class ViewHolder {
		   
	    public ImageView reminder_main_filter_image;
	    public TextView reminder_main_name_text;
	    
	    
	  }

	
	  

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    View rowView = convertView;
	    
	    
	    if (rowView == null) {
	    	
	    	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	
	      rowView = inflater.inflate(R.layout.reminder_main_list_item, null);
	      
	      ViewHolder viewHolder = new ViewHolder();
	     
	      viewHolder.reminder_main_filter_image = (ImageView) rowView.findViewById(R.id.reminder_main_filter_image);
	      viewHolder.reminder_main_name_text = (TextView) rowView.findViewById(R.id.reminder_main_name_text);
	      
	      rowView.setTag(viewHolder);
	    }
	    
	    

	    final ViewHolder holder = (ViewHolder) rowView.getTag();
	  
    	if(!reminder_filter_list.get(position).getPhoto_name().equals("")){
    		
    		holder.reminder_main_filter_image.setImageURI(Uri.parse(reminder_filter_list.get(position).getPhoto_name()));
    	}
    	
    	holder.reminder_main_name_text.setText(reminder_filter_list.get(position).getMedicine_name()+reminder_filter_list.get(position).getTaker_name());
    	
    	
    	
	    
	    
	   

	    return rowView;
	  
	} 
	  
	  
	
	
}
