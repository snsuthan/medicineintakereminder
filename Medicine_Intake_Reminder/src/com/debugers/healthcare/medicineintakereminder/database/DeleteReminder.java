package com.debugers.healthcare.medicineintakereminder.database;

import java.io.File;
import java.util.ArrayList;

import com.debugers.healthcare.medicineintakereminder.NotificationDisplay;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.widget.Toast;

public class DeleteReminder extends DatabaseHelper {
	
	ViewReminder notification;
	Context context;
	

	public DeleteReminder(Context context) {
		
		super(context);
		// TODO Auto-generated constructor stub
		this.context=context;
	}
	public void deleteReminder (long reminder_id,String photo_name)
	  {
		notification = new ViewReminder(context);
		notification.getWritableDatabase();
	   ArrayList<Integer> notificationList = notification.getNotificationIDList(reminder_id);
	   notification.close();
		for(int x=0;x<notificationList.size();x++){
			
			 Intent intent = new Intent(context,NotificationDisplay.class);
				
				PendingIntent pendingIntent = PendingIntent.getBroadcast(context,notificationList.get(x), intent, 0);
				AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
				alarmManager.cancel(pendingIntent);
			
		}
		if(!Uri.parse("R.drawable.ic_launcher").toString().equals(photo_name)){
			
			File file=new File(photo_name);
			
			Toast.makeText(context,file.delete()?"photo is delete":"not delete" , Toast.LENGTH_LONG).show();
		
		}
		SQLiteDatabase db=this.getWritableDatabase();
	   db.delete("reminder_main","reminder_id"+"=?", new String [] {String.valueOf(reminder_id)});
	   db.close();
	  }

}
