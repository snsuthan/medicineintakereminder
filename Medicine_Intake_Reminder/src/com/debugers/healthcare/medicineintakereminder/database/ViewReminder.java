package com.debugers.healthcare.medicineintakereminder.database;

import java.util.ArrayList;
import java.util.HashMap;

import com.debugers.healthcare.medicineintakereminder.NotificationDetailsPack;
import com.debugers.healthcare.medicineintakereminder.ReminderDataPack;

import android.app.AlarmManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class ViewReminder extends DatabaseHelper{
	
	
	
	public final static String TAG_TAKER_NAME_CASE="taker_name";
	public final static String TAG_MEDICINE_NAME_CASE="medicine_name";
	
	
	final static String TAG_NOTIFACATION_TIME="time";
	final static String TAG_NOTIFACATION_ID="notification_id";
	final static String TAG_REMINDER_ID="reminder_id";
	
	
	protected ArrayList<ReminderDataPack> reminder_list = new ArrayList<ReminderDataPack>();
	
	protected ArrayList<NotificationDetailsPack> notification_list = new ArrayList<NotificationDetailsPack>();
	
	protected ArrayList<ReminderDataPack> filter_list = new ArrayList<ReminderDataPack>();
	
	public ViewReminder(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ArrayList<ReminderDataPack> getReminderList(String filter_option) {
		// TODO Auto-generated method stub
		
		reminder_list.clear();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from reminder_main WHERE "+filter_option+" ORDER BY reminder_id ASC", null);
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					ReminderDataPack item = new ReminderDataPack();
					
					item.setReminder_ld(cursor.getLong(cursor.getColumnIndex("reminder_id")));
					item.setTaker_name(cursor.getString(cursor.getColumnIndex("taker_name")));
					item.setMedicine_name(cursor.getString(cursor.getColumnIndex("medicine_name")));
					item.setPhoto_name(cursor.getString(cursor.getColumnIndex("photo_name")));
					item.setQuantity_per_time(cursor.getInt(cursor.getColumnIndex("quantity_per_time")));
					item.setSchedule_data(cursor.getString(cursor.getColumnIndex("schedule_data")));
					item.setSchedule_type_index(cursor.getInt(cursor.getColumnIndex("schedule_type_index")));
					item.setTake_form_type_index(cursor.getInt(cursor.getColumnIndex("take_form_type_index")));
					item.setStart_date(cursor.getLong(cursor.getColumnIndex("start_date")));
					item.setEnd_date(cursor.getInt(cursor.getColumnIndex("end_date")));
					

					reminder_list.add(item);

				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return reminder_list;
		
	}
	
	
	public ArrayList<ReminderDataPack> getReminderFilterList(String filter_option){
		
		
		filter_list.clear();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select DISTINCT "+filter_option+" , photo_name from reminder_main ORDER BY reminder_id ASC", null);
		
		
		boolean temp_status=filter_option.equals(TAG_MEDICINE_NAME_CASE);
		
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					ReminderDataPack item = new ReminderDataPack();
					
					if(temp_status){
						item.setPhoto_name(cursor.getString(cursor.getColumnIndex("photo_name")));
						item.setMedicine_name(cursor.getString(cursor.getColumnIndex("medicine_name")));
						
					}else{
						
						item.setTaker_name(cursor.getString(cursor.getColumnIndex("taker_name")));
						
					}

					
					

					filter_list.add(item);

				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return filter_list;
		
		
		
	}
	
	
	
	public NotificationDetailsPack getNotificationReminderDetails(int  notifaction_id) {
		// TODO Auto-generated method stub
		
		NotificationDetailsPack detailsPack=new NotificationDetailsPack();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM reminder_main AS r JOIN reminder_notification AS n ON n.reminder_id =r.reminder_id AND n.notification_id=? LIMIT 1", new String[] {notifaction_id+""});
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					
					ReminderDataPack item = new ReminderDataPack();
					
					item.setReminder_ld(cursor.getLong(cursor.getColumnIndex("reminder_id")));
					item.setTaker_name(cursor.getString(cursor.getColumnIndex("taker_name")));
					item.setMedicine_name(cursor.getString(cursor.getColumnIndex("medicine_name")));
					item.setPhoto_name(cursor.getString(cursor.getColumnIndex("photo_name")));
					item.setQuantity_per_time(cursor.getInt(cursor.getColumnIndex("quantity_per_time")));
					item.setSchedule_data(cursor.getString(cursor.getColumnIndex("schedule_data")));
					item.setSchedule_type_index(cursor.getInt(cursor.getColumnIndex("schedule_type_index")));
					item.setTake_form_type_index(cursor.getInt(cursor.getColumnIndex("take_form_type_index")));
					item.setStart_date(cursor.getLong(cursor.getColumnIndex("start_date")));
					item.setEnd_date(cursor.getInt(cursor.getColumnIndex("end_date")));
					
					detailsPack.setReminderDataPack(item);
					detailsPack.setNotification_time(cursor.getInt(cursor.getColumnIndex("notification_time")));
					detailsPack.setDelay_time(cursor.getInt(cursor.getColumnIndex("delay_time")));
					detailsPack.setReminder_status(cursor.getInt(cursor.getColumnIndex("reminder_status")));
					detailsPack.setTaker_feel(cursor.getString(cursor.getColumnIndex("taker_feel")));
					
					
				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return detailsPack;
		
	}
	
	
	
	
	
	public ArrayList<NotificationDetailsPack> getNotificationList(long view_start_date) {
		// TODO Auto-generated method stub
		
		notification_list.clear();
		
		NotificationDetailsPack detailsPack=new NotificationDetailsPack();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT * FROM reminder_main AS r JOIN reminder_notification AS n ON n.reminder_id =r.reminder_id AND ( n.notification_time > ? AND n.notification_time <= ? )", 
				new String[] {String.valueOf(view_start_date),String.valueOf(view_start_date+AlarmManager.INTERVAL_DAY)});
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					ReminderDataPack item = new ReminderDataPack();
					
					item.setReminder_ld(cursor.getLong(cursor.getColumnIndex("reminder_id")));
					item.setTaker_name(cursor.getString(cursor.getColumnIndex("taker_name")));
					item.setMedicine_name(cursor.getString(cursor.getColumnIndex("medicine_name")));
					item.setPhoto_name(cursor.getString(cursor.getColumnIndex("photo_name")));
					item.setQuantity_per_time(cursor.getInt(cursor.getColumnIndex("quantity_per_time")));
					item.setTake_form_type_index(cursor.getInt(cursor.getColumnIndex("take_form_type_index")));
					
					detailsPack.setReminderDataPack(item);
					detailsPack.setNotification_id(cursor.getInt(cursor.getColumnIndex("notification_id")));
					detailsPack.setNotification_time(cursor.getInt(cursor.getColumnIndex("notification_time")));
					detailsPack.setDelay_time(cursor.getInt(cursor.getColumnIndex("delay_time")));
					detailsPack.setReminder_status(cursor.getInt(cursor.getColumnIndex("reminder_status")));
					
					

				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return notification_list;
		
	}
	
	
	
	
	public ArrayList<Integer> getNotificationIDList(long reminder_id) {
		// TODO Auto-generated method stub
		
		
		ArrayList<Integer> notifi_id_list=new ArrayList<Integer>();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select notification_id from reminder_notification WHERE reminder_id=?",new String[] {String.valueOf(reminder_id)});
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					
					notifi_id_list.add(cursor.getInt(cursor.getColumnIndex("notification_id")));
					
					

				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return notifi_id_list;
		
	}
	
	
	
	
	public ArrayList<Long> getDosageTimeList(long reminder_id) {
		// TODO Auto-generated method stub
		
		
		ArrayList<Long> time_list=new ArrayList<Long>();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select time from dosage_time WHERE reminder_id=?",new String[] {String.valueOf(reminder_id)});
		if (cursor.getCount() != 0) {
			if (cursor.moveToFirst()) {
				do {
					 time_list.add(cursor.getLong(cursor.getColumnIndex("time")));
				

				} while (cursor.moveToNext());
			}
		}
		cursor.close();
		db.close();
		return time_list;
		
	}
	
	
	
	
	
	

}
