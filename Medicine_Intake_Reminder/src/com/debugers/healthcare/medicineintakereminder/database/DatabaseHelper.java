package com.debugers.healthcare.medicineintakereminder.database;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class DatabaseHelper extends SQLiteOpenHelper {

	public static String DATABASENAME = "healthcare";
	
	
	Context c;

	public DatabaseHelper(Context context) {
		super(context, DATABASENAME, null, 33);
		c = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		
		//Reminder Main Table create

		db.execSQL("CREATE TABLE reminder_main (schedule_data TEXT, quantity_per_time INTEGER, reminder_id INTEGER PRIMARY KEY, taker_name text, medicine_name text, start_date INTEGER, "+
				"end_date INTEGER, take_form_type_index INTEGER, schedule_type_index INTEGER, photo_name text, added_on TIMESTAMP)");
		
		
		
		// Dosage Time  table 
		
		db.execSQL("CREATE TABLE if not exists dosage_time(reminder_id INTEGER NOT NULL,time INTEGER NOT NULL)");
		
		
		//Notification table
		
		db.execSQL("CREATE TABLE if not exists reminder_notification(reminder_id INTEGER NOT NULL,notification_id text,notification_time text , reminder_status INTEGER, delay_time INTEGER ,taker_feel text  )");
		
		
		
		// Trigger table 
		
		db.execSQL("CREATE TRIGGER reminder_delete BEFORE DELETE ON reminder_main " +
				"FOR EACH ROW BEGIN " +
				"DELETE from dosage_time WHERE reminder_id = OLD.reminder_id; " +
				"DELETE from reminder_notification WHERE reminder_id = OLD.reminder_id; END");
		
		

	}
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		onCreate(db);
	}

	
	

	
	
}
