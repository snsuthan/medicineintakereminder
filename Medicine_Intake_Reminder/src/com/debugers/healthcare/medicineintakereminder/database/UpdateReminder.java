package com.debugers.healthcare.medicineintakereminder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class UpdateReminder  extends DatabaseHelper{

	public UpdateReminder(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		
	}
	
	
	
	public void setNotificationTakeFeel(int notification,String take_feel){
		
		
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put("reminder_status",1); 
	    values.put("taker_feel", take_feel); 
	    db.update("reminder_notification", values,"notification_id = ?", new String[] { String.valueOf(notification) }); 
	 
	    db.close();
	}
	
	
	
public void setNotificationDelayTime(int notification_id,int delay_time){
		
		
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    ContentValues values = new ContentValues();
	    values.put("delay_time",delay_time);
	    db.update("reminder_notification", values,"notification_id = ?", new String[] { String.valueOf(notification_id) }); 
	 
	    db.close();
	}
	
	
	
	
	

}
