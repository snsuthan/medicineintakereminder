package com.debugers.healthcare.medicineintakereminder.database;


import java.util.ArrayList;
import java.util.HashMap;

import com.debugers.healthcare.medicineintakereminder.ReminderDataPack;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class AddReminder extends DatabaseHelper {
	
	
	
	ArrayList<HashMap<String, Long>> notification_list;
	ArrayList<Long> time_schedule_list;
	
	
	ReminderDataPack reminderDataPack;
	
	
	
	final static String TAG_NOTIFACATION_TIME="time";
	final static String TAG_NOTIFACATION_ID="notification_id";
	
	
	

	public AddReminder(Context context,ArrayList<HashMap<String, Long>> notification_list,ArrayList<Long> time_schedule_list,ReminderDataPack reminderDataPack) {
		super(context);
		// TODO Auto-generated constructor stub
		
		this.notification_list=notification_list;
		this.time_schedule_list=time_schedule_list;
		this.reminderDataPack=reminderDataPack;
		
		addReminder() ;
		
		
	}
	
	
	

	public void addReminder() {
		
		
		// TODO Auto-generated method stub
		
		
		
		long reminder_id=reminderDataPack.getReminder_ld();
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues reminder_main = new ContentValues();
		
	       reminder_main.put("reminder_id",reminder_id);
	       reminder_main.put("taker_name",reminderDataPack.getTaker_name());
	       reminder_main.put("medicine_name",reminderDataPack.getMedicine_name());
	       reminder_main.put("schedule_data",reminderDataPack.getSchedule_data());
	       reminder_main.put("take_form_type_index",reminderDataPack.getTake_form_type_index());
	       reminder_main.put("schedule_type_index",reminderDataPack.getSchedule_type_index());
	       reminder_main.put("quantity_per_time",reminderDataPack.getQuantity_per_time());
	       reminder_main.put("start_date",reminderDataPack.getStart_date());
	       reminder_main.put("end_date",reminderDataPack.getEnd_date());
	       reminder_main.put("photo_name",reminderDataPack.getPhoto_name());
	       
		
		db.insert("reminder_main", null, reminder_main);
		db.close();
		
		
		for (long  time : time_schedule_list) {
			
			
			SQLiteDatabase db_time = this.getWritableDatabase();
			ContentValues dosage_time = new ContentValues();
			
			dosage_time.put("reminder_id",reminder_id);
			dosage_time.put("time",time);
			
			db_time.insert("dosage_time", null, dosage_time);
			db_time.close();
			
			
		}
		
		
		for (HashMap<String, Long> notifi : notification_list) {
			
			SQLiteDatabase db5 = this.getWritableDatabase();
			ContentValues notification = new ContentValues();
			
			notification.put("reminder_id",reminder_id);
			notification.put("notification_id",notifi.get(TAG_NOTIFACATION_ID));
			
			notification.put("notification_time",notifi.get(TAG_NOTIFACATION_TIME));
			notification.put("reminder_status",0);  //Not Taken
			notification.put("delay_time",0);
			notification.put("taker_feel","");
			
			
			
			db5.insert("reminder_notification", null, notification);
			db5.close();
			
			
			
		}
		
			
		
		
	}

	
	
	
	
	
}
