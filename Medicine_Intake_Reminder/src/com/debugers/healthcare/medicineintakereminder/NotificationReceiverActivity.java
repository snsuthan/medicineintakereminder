package com.debugers.healthcare.medicineintakereminder;

import com.debugers.healthcare.medicineintakereminder.database.UpdateReminder;
import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

public class NotificationReceiverActivity extends Activity {
	
	
	
	final static String TAG_NOTIFACATION_ID="notification_id";
	
	int notification_id;
	
	final Context context = this;

	NotificationDetailsPack notifation_details;
	ReminderDataPack reminder_data;
	
	UpdateReminder updateReminder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Intent intent=getIntent();
		notification_id=intent.getIntExtra(TAG_NOTIFACATION_ID, 0); 
		
		ViewReminder reminder=new ViewReminder(context);
		
		updateReminder=new UpdateReminder(context);
		
		notifation_details = reminder.getNotificationReminderDetails(notification_id);
		reminder_data=notifation_details.getReminderDataPack();
		
		final Dialog dialog = new Dialog(context);
    	dialog.setTitle("Notification View");
    	dialog.setContentView(R.layout.notification_dialog_view);
    	
		// TODO Auto-generated method stub
    	
    	
    	TextView notification_taker_name=(TextView) dialog.findViewById(R.id.notification_taker_name);
    	TextView notification_medicine_name=(TextView) dialog.findViewById(R.id.notification_medicine_name);
    	TextView notification_medicine_size=(TextView) dialog.findViewById(R.id.notification_size_of_medicine);
    	
    	
    	final EditText notification_taker_feel=(EditText) dialog.findViewById(R.id.notification_taker_feel);
    	
    	ImageView notification_photo=(ImageView) dialog.findViewById(R.id.notification_photo_of_medicine);
    	
    	Button notification_reminder_cancel_button=(Button) dialog.findViewById(R.id.notification_cancel_button);
    	Button notification_reminder_skip_button=(Button) dialog.findViewById(R.id.notification_skip_button);
    	Button notification_reminder_take_button=(Button) dialog.findViewById(R.id.notification_take_button);
    	
    	
    	notification_taker_name.setText(reminder_data.getTaker_name());
    	notification_medicine_name.setText(reminder_data.getMedicine_name());
    	notification_medicine_size.setText(reminder_data.getQuantity_per_time()+" "+ 
    	context.getResources().getStringArray(R.array.medicineTaketype)[reminder_data.getTake_form_type_index()]);
    	notification_photo.setImageURI(Uri.parse(reminder_data.getPhoto_name()));
    	
    	
    	notification_photo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	
            		final Dialog photo_dialog = new Dialog(NotificationReceiverActivity.this);
            		photo_dialog.setTitle("Medicine Photo View");
            		photo_dialog.setContentView(R.layout.dialog_medicine_photo_full_view);
            		
                	ImageView dialog_medicine_full_view_photo=(ImageView) photo_dialog.findViewById(R.id.dialog_medicine_full_view_photo);
                	Button dialog_full_view_btn =(Button) photo_dialog.findViewById(R.id.dialog_full_view_btn);
                	dialog_medicine_full_view_photo.setImageURI(Uri.parse(reminder_data.getPhoto_name()));
                	
                	dialog_full_view_btn.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                        	
                        	photo_dialog.cancel();
                           


                        }
                    });
                	
                	
                	photo_dialog.show();
            		
            	
            	 ;}
            });
    	
    	
    	notification_reminder_take_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	
            	updateReminder.setNotificationTakeFeel(notification_id, notification_taker_feel.getText().toString());
            	
            	dialog.cancel();
	            finish();
            	
            	
            	
            	
            	 ;}
            });
    	
    	
    	notification_reminder_skip_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	TimePickerDialog.OnTimeSetListener time=new TimePickerDialog.OnTimeSetListener() {
					   
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						long time=System.currentTimeMillis()+(hourOfDay*60+minute)*60000;
						
						Intent intent = new Intent(context,NotificationDisplay.class);
						intent.putExtra(TAG_NOTIFACATION_ID,notification_id);
						PendingIntent pendingIntent = PendingIntent.getBroadcast(context,(int)System.currentTimeMillis()/10000, intent,PendingIntent.FLAG_CANCEL_CURRENT);
						AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
						alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
						
						updateReminder.setNotificationDelayTime(notification_id,notifation_details.getDelay_time()+(hourOfDay*60+minute)*60000);
						
						dialog.cancel();
			            finish();
				      
					     }};  
		            new TimePickerDialog(context, time,0,5,true).show();
		            
				
			    }
		});
            	
            	
            	
    	
    	
    	notification_reminder_cancel_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	dialog.cancel();
	            finish();
            	
            	 ;}
            });
    	
    	
    	
    	
    	
    	dialog.show();
		
		
		
		
	}
	

}
