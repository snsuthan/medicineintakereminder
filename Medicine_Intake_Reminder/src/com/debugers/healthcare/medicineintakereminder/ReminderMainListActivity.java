package com.debugers.healthcare.medicineintakereminder;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ListView;

import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

public class ReminderMainListActivity extends Activity{
	
	
	ArrayList<ReminderDataPack> reminder_filter_list;
	
	ViewReminder viewReminder;
	ListView listview;
	
	public final static String TAG_TAKER_NAME_CASE="taker_name";
	public final static String TAG_MEDICINE_NAME_CASE="medicine_name";
	
	public final static String TAG_FILTER_OPTION="filter";
	
	
	String filter_option="";
	
	
	
	CalendarView calendarView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_main_list_activty);
		
		listview = (ListView) findViewById(R.id.reminder_list_view);
		
		
	     calendarView=(CalendarView) findViewById(R.id.reminder_date_calender_view);
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				
				Intent intent=new Intent(getApplicationContext(),ReminderNameSortListViewActivity.class);
	           
				
				if(filter_option.equals(TAG_MEDICINE_NAME_CASE)){
					
					intent.putExtra(TAG_FILTER_OPTION, TAG_MEDICINE_NAME_CASE+"='"+reminder_filter_list.get(pos).getMedicine_name()+"' ");
					
				}else if(filter_option.equals(TAG_TAKER_NAME_CASE)){
					
					intent.putExtra(TAG_FILTER_OPTION, TAG_TAKER_NAME_CASE+"='"+reminder_filter_list.get(pos).getTaker_name()+"' ");
				}
				 startActivity(intent);
				
				
				
				
			}
			
		});
		
		
		
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, 
        		getApplicationContext().getResources().getStringArray(R.array.reminder_main_filter_option));
 
       
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
 
        ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
 
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
            	
            	listview.setVisibility(View.GONE);
            	calendarView.setVisibility(View.GONE);
            	switch (itemPosition) {
				case 0: // Patient
					
					filter_option=TAG_TAKER_NAME_CASE;
					selectSortOption();
					
					break;
				case 1:  // Medicine
					
					filter_option=TAG_MEDICINE_NAME_CASE;
					selectSortOption();
									
					break;
				
				case 2:  // Date
					listview.setVisibility(View.GONE);
	            	calendarView.setVisibility(View.VISIBLE);
					
					break;


				default:
					break;
				}
            	
            	
            	
                return false;
            }
        };
 
       
        getActionBar().setListNavigationCallbacks(adapter, navigationListener);
        
        
		
        calendarView.setOnDateChangeListener(new OnDateChangeListener() {
			
			@Override
			public void onSelectedDayChange(CalendarView arg0, int year, int month,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				
				Calendar temp_change_date = Calendar.getInstance();
				temp_change_date.set(year,month,dayOfMonth);
				
				Intent intent=new Intent(getApplicationContext(),ReminderDateSortListActivity.class);
				intent.putExtra("get_date",temp_change_date.getTimeInMillis());
				 startActivity(intent);
				
			}
		});
        
        
        
        filter_option=TAG_TAKER_NAME_CASE;
        selectSortOption();
        
        
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		
		getMenuInflater().inflate(R.menu.new_reminder_menu, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		if(item.getItemId()==R.id.add_new){
			
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	
	
	public void selectSortOption(){
		
		listview.setVisibility(View.VISIBLE);
    	calendarView.setVisibility(View.GONE);
		
		viewReminder=new ViewReminder(ReminderMainListActivity.this);
		viewReminder.getWritableDatabase();
		reminder_filter_list=viewReminder.getReminderFilterList(filter_option);
		
		RemiderMainListAdapter listArrayAdapter=new RemiderMainListAdapter(ReminderMainListActivity.this, reminder_filter_list);
		listview.setAdapter(listArrayAdapter);
		
		
		
	}
	
	

}
