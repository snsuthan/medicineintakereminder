package com.debugers.healthcare.medicineintakereminder;

import java.util.ArrayList;

import com.debugers.healthcare.medicineintakereminder.database.ViewReminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationRemove extends BroadcastReceiver {
	
	
	ViewReminder notification;
	
	final static String TAG_REMINDER_ID="reminder_id";
	

	@Override
	public void onReceive(Context arg0, Intent inten) {
		// TODO Auto-generated method stub
		
		notification=new ViewReminder(arg0);
		notification.getWritableDatabase();
		
		ArrayList<Integer> notificationList = notification.getNotificationIDList(inten.getLongExtra(TAG_REMINDER_ID, 0));
		
		for(int x=0;x<notificationList.size();x++){
            Intent intent = new Intent(arg0,NotificationDisplay.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(arg0,notificationList.get(x), intent, 0);
			AlarmManager alarmManager = (AlarmManager)arg0.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			
		}

	}

}
